package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int sum = 0;
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String results = "";

        results += s;
        while (results.length() > 1) {
            char[] c = results.toCharArray();
            results = "";
            for (int i = 0; i < c.length; i++) {

                if (i + 1 < c.length) {
                    int a = Integer.parseInt(String.valueOf(c[i]));
                    int b = Integer.parseInt(String.valueOf(c[i + 1]));
                    sum = a + b;
                    results += String.valueOf(sum);
                    i++;
                } else {
                    results += c[i];
                }
            }

        }
        System.out.println(results);
    }
}
